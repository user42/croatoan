﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using AForge.Video;
using AForge.Video.DirectShow;

namespace Croatoan_client
{
    public partial class Form1 : Form
    { 
        private FilterInfoCollection VideoCaptureDevices;
        private VideoCaptureDevice FinalVideo;

        public Form1()
        {
            InitializeComponent();
        } 
         
        private void button1_Click(object sender, EventArgs e)
        {
            if(FinalVideo.IsRunning == true)
            {
                FinalVideo.Stop();
                button1.Text = "Start";
                
            }
            else
            {
                FinalVideo = new VideoCaptureDevice(VideoCaptureDevices[comboBox1.SelectedIndex].MonikerString);
                FinalVideo.NewFrame += new NewFrameEventHandler(FinalVideo_NewFrame);
                FinalVideo.Start();
                button1.Text = "Stop";
            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            VideoCaptureDevices = new FilterInfoCollection(FilterCategory.VideoInputDevice);

            foreach (FilterInfo VideoCaptureDevice in VideoCaptureDevices)
            {
                comboBox1.Items.Add(VideoCaptureDevice.Name);
            }
            //int i = comboBox1.SelectedIndex;
            try
            {
                comboBox1.SelectedIndex = 0;
            }
            catch
            {
                MessageBox.Show("Нет веб-камер.", "Ошибка",0,MessageBoxIcon.Error);
                Close();
            }

            FinalVideo = new VideoCaptureDevice();
        }

        void FinalVideo_NewFrame(object sender, NewFrameEventArgs eventArgs)
        {
            Bitmap video = (Bitmap)eventArgs.Frame.Clone();
            pictureBox1.Image = video;
        }

        private void Form1_FormClosed(object sender, FormClosedEventArgs e)
        {
            try
            {
                FinalVideo.Stop();
            }
            catch { }
        }


    }
}
